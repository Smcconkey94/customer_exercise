package com.citi.customer.starter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.citi.customer.model.Customer;
import com.citi.customer.repository.CustomerRepository;

@Component
public class Starter implements ApplicationRunner {

    @Autowired
    private CustomerRepository customerRepository;

    public void run(ApplicationArguments appArgs) {
        System.out.println("Creating some customers");
        String[] customerNames = {"Dave", "Mike", "Bob"};
        String[] customerAddresses = {"Belfast", "Lisburn", "Dublin"};

        for(int i=0; i<customerNames.length; ++i) {
            Customer thisCustomer = new Customer(i,
                                              customerNames[i],
            								customerAddresses[i]);

            System.out.println("Created Product: " + thisCustomer);

            customerRepository.saveCustomer(thisCustomer);
        }

        System.out.println("\nAll Customers:");
        for(Customer customer: customerRepository.getAllCustomers()) {
            System.out.println(customer);
        }
  
    }

    }