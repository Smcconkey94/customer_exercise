package com.citi.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerExerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerExerciseApplication.class, args);
	}

}
