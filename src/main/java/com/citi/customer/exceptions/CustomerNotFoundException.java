package com.citi.customer.exceptions;

public class CustomerNotFoundException extends RuntimeException{

	private String error = "Customernot found";
	
	public CustomerNotFoundException(String error) {
		super(error);
	}
	
}
