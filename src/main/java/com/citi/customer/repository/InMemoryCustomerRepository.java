package com.citi.customer.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.citi.customer.exceptions.CustomerNotFoundException;
import com.citi.customer.model.Customer;


@Component
public class InMemoryCustomerRepository implements CustomerRepository{

    private Map<Integer, Customer> allCustomers = new HashMap<Integer, Customer>();

	@Override
	public void saveCustomer(Customer customer) {
        allCustomers.put(customer.getId(), customer);
	}

	@Override
	public void deleteCustomer(int id) {
		Customer foundCustomer = allCustomers.remove(id);
    	if (foundCustomer == null) {
    		throw new CustomerNotFoundException("Customer Not Found: " + id + " Delete Customer");
		}
    }

	@Override
	public Customer getCustomer(int id) {
		Customer foundCustomer = allCustomers.get(id);
    	if (foundCustomer != null) {
			return foundCustomer;
		} else throw new CustomerNotFoundException("Customer Not Found: " + id + " Get Customer");
	}

	@Override
	public List<Customer> getAllCustomers() {
        return new ArrayList<Customer>(allCustomers.values());

	}

}
