package com.citi.customer.repository;

import java.util.List;

import com.citi.customer.model.Customer;

public interface CustomerRepository {
	
	void saveCustomer(Customer customer);
    
    void deleteCustomer(int id);

    Customer getCustomer(int id);

    List<Customer> getAllCustomers();

}
