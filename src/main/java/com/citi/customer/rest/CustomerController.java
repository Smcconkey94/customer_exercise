package com.citi.customer.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.customer.model.Customer;
import com.citi.customer.repository.CustomerRepository;

@RestController
@RequestMapping("/customer")
public class CustomerController {
	private static Logger LOG = LoggerFactory.getLogger(CustomerController.class);
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@RequestMapping(method=RequestMethod.GET)
	public List<Customer> getAll(){
		return customerRepository.getAllCustomers();
	}
	
	@RequestMapping(method=RequestMethod.GET, path="{id}")
	public ResponseEntity<Customer> getProductById(@PathVariable("id") int id) {
		LOG.info("Get by ID called");
		Customer foundCustomer = customerRepository.getCustomer(id);
		return new ResponseEntity<Customer>(foundCustomer, HttpStatus.OK);	
	}
	
	@RequestMapping(method=RequestMethod.POST,
					produces=MediaType.APPLICATION_JSON_VALUE,
					consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Customer> saveProduct(@RequestBody Customer customer) {
		LOG.info("POST - Save Customer called: " + customer);
		customerRepository.saveCustomer(customer);
		return new ResponseEntity<Customer>(customer, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/{id}",
			method=RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable int id) {
		LOG.info("delete was called: " + id);
		customerRepository.deleteCustomer(id);
	}
	
}

