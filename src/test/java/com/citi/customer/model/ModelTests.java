package com.citi.customer.model;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.Assert;

public class ModelTests {

	Customer c1 = new Customer(1, "Jonny", "Belfast");

	@Test
	public void ConstructorTest() {
		Customer c1 = new Customer(1, "Jonny", "Belfast");

			assertEquals(c1.getId(), 1);
			assertEquals(c1.getName(), "Jonny");
			assertEquals(c1.getAddress(), "Belfast");		
	}
	
	@Test
	public void nameSetterTest() {
		Customer c2 = new Customer(1, "Jonny2", "Belfast");

			c2.setName("Jonny2");
			
			assertEquals("Jonny2", c2.getName());
	}
	
	@Test
	public void nameGetterTest() {
		Customer c3 = new Customer(1, "Jonny3", "Belfast");

			assertEquals(c3.getName(), "Jonny3");
	}

}
